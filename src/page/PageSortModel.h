/*
 * This file is part of KSysGuard.
 * Copyright 2019 Arjen Hiemstra <ahiemstra@heimr.nl>
 * Copyright 2020 David Redondo <kde@david-redondo.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAGESORTMODEL_H
#define PAGESORTMODEL_H

#include <QAbstractProxyModel>

#include "PagesModel.h"

class PageSortModel : public QAbstractProxyModel
{
    Q_OBJECT

public:

    enum Roles {
        ShouldRemoveFilesRole = PagesModel::FilesWriteableRole + 1
    };
    Q_ENUM(Roles)

    explicit PageSortModel(QObject* parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    void setSourceModel(QAbstractItemModel* newSourceModel) override;

    QModelIndex index(int row, int column, const QModelIndex &parent ) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;
    QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    Q_INVOKABLE void move(int fromRow, int toRow);

    Q_INVOKABLE void applyChangesToSourceModel() const;
private:
    QVector<int> m_rowMapping;
    QVector<bool> m_hiddenProxy;
    QVector<bool> m_removeFiles;

};

#endif
